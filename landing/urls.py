
from django.contrib import admin
from django.urls import path
from catalog.views import auth_page, auth_go
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', auth_page),
    path('auth/', auth_go)
]
