
function go_button(event) {
	name  = document.getElementById('name').value
	email = document.getElementById('email').value
	phone = document.getElementById('phone').value

	if (name=='' || email=='' || phone =='') {
		alert('Заполните все поля')
		return
	}

	data = JSON.stringify({'name': name, 'email': email, 'phone': phone})
	$.ajax( {
		type: 'POST',
	 	url:  '/auth/',
		data: data,
		success: function (result) {
			alert(result);         
		}
	})
}