from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
import json
import psycopg2

def auth_page(request):
	return render(request, 'index.html')

@csrf_exempt
def auth_go(request):
	if request.method != 'POST':
		return HttpResponse('Ошибка запроса')	

	data = json.loads(request.body)
	
	base = psycopg2.connect('user=postgres dbname=landing password=qwerasdf host=127.0.0.1 port=5432')
	cursor = base.cursor()
	q_text = '''
		INSERT INTO users (name, email, phone)
		VALUES (%s, %s, %s)'''
	cursor.execute(q_text, (data['name'], data['email'], data['phone']))
	base.commit()

	return HttpResponse('Готово')

	